# Komoot test

## About the technical test

### Posible improvements

#### Better content style

It would look way more integrated with the actual app if I used the app's style.

#### Accessibility and responsive

#### React prop types setup

I didn't define them for the demo.

#### More testing

**E2E testing** is very important for apps like this one to make sure that the app sections do not fail and work as expected.

More **unit/component testing** to cover every possible case. Due it's just a demo, not even an MVP, I wrote very very little. Just the necessary so I could show off my skills.

#### Git + CI/CD

#### Use of Trello (or similar) to break down the task into smaller ones

#### environment files

#### Separation of concerns

Not the case at all, but if this project had a bigger aim, we could have used Storybook/DocZ or similar. We could have separated the services into different packages, created a documentation for each of their methods using JSDoc...etc

#### Typescript

## Available Scripts

### npm start

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### npm test

Launches the test runner in the interactive watch mode.
See the section about running tests for more information.

### npm run build

Builds a static copy of your site to the `build/` folder.
Your app is ready to be deployed!

**For the best production performance:** Add a build bundler plugin like "@snowpack/plugin-webpack" or "@snowpack/plugin-parcel" to your `snowpack.config.json` config file.

- - -

> ✨ Bootstrapped with Create Snowpack App (CSA).