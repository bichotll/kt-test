// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import "@testing-library/jest-dom/extend-expect";


jest.mock('mapbox-gl/dist/mapbox-gl', () => {
  const markerInstanceFunctions = {
    setLngLat: jest.fn(function () { return this }),
    addTo: jest.fn(function () { return this }),
    on: jest.fn(function () { return this }),
    remove: jest.fn(function () { return this }),
    getLngLat: jest.fn(function () { return this }),
  }

  return {
    markerInstanceFunctions,
    Marker: jest.fn(() => ({
      setLngLat: markerInstanceFunctions.setLngLat,
      addTo: markerInstanceFunctions.addTo,
      on: markerInstanceFunctions.on,
      remove: markerInstanceFunctions.remove,
      getLngLat: markerInstanceFunctions.getLngLat,
    })),
    GeolocateControl: jest.fn(),
    Map: jest.fn(() => ({
      addControl: jest.fn(),
      on: jest.fn(),
      remove: jest.fn()
    })),
    NavigationControl: jest.fn()
  }
})
