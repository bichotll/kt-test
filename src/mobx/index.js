import { configure } from 'mobx'

import Route from './stores/route'

configure({ enforceActions: 'observed' })

class RootStore {
  route = Route
}

window.rootStore = new RootStore()

export default window.rootStore
