import { makeObservable, observable, action } from "mobx"

const defaultPoints = [
  {
    id: 'Waypoint 1',
    name: 'Waypoint 1',
    lng: 2.835087,
    lat: 41.986989,
  },
  {
    id: 'Waypoint 2',
    name: 'Waypoint 2',
    lng: 2.837087,
    lat: 41.987989,
  },
  {
    id: 'Waypoint 3',
    name: 'Waypoint 3',
    lng: 2.838087,
    lat: 41.986989,
  },
  {
    id: 'Waypoint 4',
    name: 'Waypoint 4',
    lng: 2.842087,
    lat: 41.986989,
  },
  {
    id: 'Waypoint 5',
    name: 'Waypoint 5',
    lng: 2.842087,
    lat: 41.984989,
  },
]

class Route {
  points

  isSelectingNewPoint

  constructor() {
    makeObservable(this, {
      points: observable,
      isSelectingNewPoint: observable,
      setIsSelectingNewPoint: action,
      addSpot: action,
      deleteSpot: action,
      moveSpotPosition: action,
    })

    this.points = [...defaultPoints]
    this.isSelectingNewPoint = false
  }

  setIsSelectingNewPoint (isSelectingNewPoint) {
    this.isSelectingNewPoint = isSelectingNewPoint
  }

  addSpot (lng, lat) {
    const id = `Waypoint ${this.points.length + 1}`
    this.points = [
      ...this.points,
      {
        id,
        name: id,
        lng,
        lat,
      },
    ]
  }

  moveSpotPosition (fromIndex, toIndex) {
    const pointToMove = this.points[fromIndex]
    const modPointsArray = [...this.points]
    modPointsArray.splice(fromIndex, 1)
    modPointsArray.splice(toIndex, 0, pointToMove)

    this.points = [...modPointsArray]
  }

  modifySpotCoords (point, lng, lat) {
    const arrayPoint = this.points.find((iteratedPoint) => iteratedPoint.id === point.id)
    arrayPoint.lng = lng
    arrayPoint.lat = lat
    this.points = [
      ...this.points,
    ]
  }

  deleteSpot (id) {
    this.points = [...this.points.filter((point) => point.id !== id)]
  }
}

export default new Route()
