import React from 'react';
import './App.css';

import Sidebar from './ui/Sidebar'
import Map from './ui/Map/container'

function App() {
  return (
    <div className="App container-fluid vh-100">
        <div className="row vh-100">
          <div className="col-3 p-0 vh-100 bg-dark">
            <Sidebar />
          </div>
          <div className="col-9 p-0 vh-100">
            <Map />
          </div>
        </div>
    </div>
  );
}

export default App;
