import React, { PureComponent } from 'react'
import GPX from './../../../services/gpx'


class Download extends PureComponent {
  donwloadGPX = () => {
    const { points } = this.props
    const fileContent = GPX.generateGPXFile(points)

    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(fileContent));
    element.setAttribute('download', 'route.gpx');
    element.style.display = 'none';

    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  render() {
    return (
      <div>
        <div
          className="btn btn-success btn-block"
          onClick={this.donwloadGPX}
        >
          Download your Route
        </div>
      </div>
    );
  }
}

export default Download;
