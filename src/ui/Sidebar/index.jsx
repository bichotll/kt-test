import React from 'react'
import RoutePlanner from './RoutePlanner/container'
import Download from './Download/container'

const Sidebar = () => {
  return (
    <div
      className="d-flex flex-column justify-content-between vh-100 p-2"
    >
      <div className="text-light route-builder">
        <h2 className="text-left">
          Route builder
        </h2>
        <hr />
        <RoutePlanner />
      </div>

      <Download />
    </div>
  )
}

export default Sidebar
