import React, { PureComponent } from 'react'
import Waypoint from './Waypoint'

import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'


class RoutePlanner extends PureComponent {
  render() {
    const {
      points,
      onDeletePoint,
      onMovePointPosition,
      isSelectingNewPoint,
      setIsSelectingNewPoint
    } = this.props

    return (
      <div className="route-planner">
        <div className={`waypoints-list ${isSelectingNewPoint && 'disabled'}`}>
          <DndProvider backend={HTML5Backend}>
            {points.map((point, index) => (
              <Waypoint
                key={point.id}
                index={index}
                point={point}
                onDelete={onDeletePoint}
                moveCard={onMovePointPosition}
              />
            ))}
          </DndProvider>
        </div>
        <div
          className="btn btn-info btn-block mt-4"
          onClick={() => {
            setIsSelectingNewPoint(!isSelectingNewPoint)
          }}
        >
          <>
            {isSelectingNewPoint ? 'Select a new point on the map' : 'Add new waypoint'}
          </>
        </div>
      </div>
    );
  }
}

export default RoutePlanner
