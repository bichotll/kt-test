import React from 'react'
import { observer } from 'mobx-react'

import Store from './../../../mobx'
import Component from './index.jsx'

const { route } = Store

export default observer(props => (
  <Component
    {...props}
    points={route.points}
    isSelectingNewPoint={route.isSelectingNewPoint}

    onDeletePoint={(id) => {
      return route.deleteSpot(id)
    }}

    onMovePointPosition={(fromIndex, toIndex) => {
      return route.moveSpotPosition(fromIndex, toIndex)
    }}

    setIsSelectingNewPoint={(isSelectingNewPoint) => {
      return route.setIsSelectingNewPoint(isSelectingNewPoint)
    }}
  />
))
