import React from 'react'
import { observer } from 'mobx-react'

import Store from './../../mobx'
import Component from './index.jsx'

const { route } = Store

export default observer(props => (
  <Component
    {...props}
    points={route.points}
    isSelectingNewPoint={route.isSelectingNewPoint}

    addSpot={(lng, lat) => {
      console.log('addspot...');
      route.addSpot(lng, lat)
      route.setIsSelectingNewPoint(false)
    }}
  />
))
