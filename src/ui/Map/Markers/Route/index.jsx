import React, { Component } from 'react'

class Route extends Component {
  componentDidMount () {
    this.bindRoute()
  }

  componentDidUpdate () {
    this.unbindRoute()
    this.bindRoute()
  }

  componentWillUnmount () {
    this.unbindRoute()
  }

  bindRoute = () => {
    this.writeRoute()
  }

  unbindRoute = () => {
    const { map } = this.props

    if (map.getLayer('route')) {
      map.removeLayer('route')
    }

    if (map.getSource('route')) {
      map.removeSource('route')
    }
  }

  writeRoute = () => {
    const { points, map } = this.props

    map.addSource('route', {
      'type': 'geojson',
      'data': {
        'type': 'Feature',
        'properties': {},
        'geometry': {
          'type': 'LineString',
          'coordinates': [
            ...points.map(({lng, lat}) => [lng, lat])
          ]
        }
      }
    });

    map.addLayer({
      'id': 'route',
      'type': 'line',
      'source': 'route',
      'layout': {
        'line-join': 'round',
        'line-cap': 'round'
      },
      'paint': {
        'line-color': '#888',
        'line-width': 8
      }
    });
  }

  render() {
    return (
      <></>
    );
  }
}

export default Route
