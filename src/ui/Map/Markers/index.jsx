import React, { PureComponent } from 'react'

import Marker from './Marker'
import Route from './Route'

class Markers extends PureComponent {
  render() {
    const { map, points, onMarkerDragEnd } = this.props

    return (
      <>
        {points.map((point) => (
          <Marker
            key={point.id}
            map={map}
            point={point}
            onDragEnd={(lng, lat) => {
              onMarkerDragEnd(point, lng, lat)
            }}
          />
        ))}

        <Route
          points={points}
          map={map}
        />
      </>
    );
  }
}

export default Markers
