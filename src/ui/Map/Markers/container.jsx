import React from 'react'
import { observer } from 'mobx-react'

import Store from './../../../mobx'
import Component from './index.jsx'

const { route } = Store

export default observer(props => (
  <Component
    {...props}
    points={route.points}
    onMarkerDragEnd={(point, lng, lat) => {
      return route.modifySpotCoords(point, lng, lat)
    }}
  />
))
