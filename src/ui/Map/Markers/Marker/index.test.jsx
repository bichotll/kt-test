import * as React from 'react';
import { render } from '@testing-library/react';
import mapboxgl from 'mapbox-gl'

import Marker from './';

const map = {
  test: 'fdsa',
}
const point = {
  lng: 'lng',
  lat: 'lat',
}


describe('Marker', () => {
  describe('Marker lifecycle events', () => {
    it('calls...', () => {
      const { unmount } = render(<Marker point={point} map={map} />)
      expect(mapboxgl.Marker).toHaveBeenCalled()
      expect(mapboxgl.markerInstanceFunctions.setLngLat).toHaveBeenCalledWith([point.lng, point.lat])
      expect(mapboxgl.markerInstanceFunctions.addTo).toHaveBeenCalledWith(map)
      expect(mapboxgl.markerInstanceFunctions.on).toHaveBeenCalledWith('dragend', expect.any(Function))

      expect(mapboxgl.markerInstanceFunctions.remove).not.toHaveBeenCalled()

      unmount()
      expect(mapboxgl.markerInstanceFunctions.remove).toHaveBeenCalled()
    })
  })

  describe('Marker object', () => {
    describe('on dragend', () => {
      it('calls prop onDragEnd', () => {
        const onDragEnd = jest.fn()
        let dragEndCallback
        mapboxgl.markerInstanceFunctions.on = jest.fn(function (eventName, eventCallback) {
          dragEndCallback = eventCallback
          return this
        })

        render(<Marker point={point} map={map} onDragEnd={onDragEnd} />)
        
        expect(mapboxgl.Marker).toHaveBeenCalled()
        expect(mapboxgl.markerInstanceFunctions.on).toHaveBeenCalledWith('dragend', expect.any(Function))
        
        expect(onDragEnd).not.toHaveBeenCalled()

        dragEndCallback()

        expect(onDragEnd).toHaveBeenCalled()
      })
    })
  })
})
