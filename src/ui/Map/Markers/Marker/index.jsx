import React, { Component } from 'react'
import mapboxgl from 'mapbox-gl'

class Marker extends Component {
  componentDidMount () {
    this.bindMarker()
  }

  componentWillUnmount () {
    this.unbindMarker()
  }

  bindMarker = () => {
    const { point, onDragEnd } = this.props

    this.marker = new mapboxgl.Marker({ draggable: true })
      .setLngLat([point.lng, point.lat])
      .addTo(this.props.map)
      .on('dragend', () => {
        const coords = this.marker.getLngLat()
        onDragEnd(coords.lng, coords.lat)
      })
  }

  unbindMarker = () => {
    this.marker.remove()
  }

  render() {
    return (
      <></>
    );
  }
}

export default Marker;