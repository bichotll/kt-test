import React, { PureComponent } from 'react'
import mapboxgl from 'mapbox-gl'

import Markers from './Markers/container'

class Map extends PureComponent {
  map = undefined
  mapboxContainerRef = React.createRef()

  state = {
    mapboxInstance: undefined,
    mapboxInstanceLoaded: false,
  }

  componentDidMount () {
    this.bindMapbox()
  }

  bindMapbox = () => {
    mapboxgl.accessToken = 'pk.eyJ1IjoiYmljaG90bGwyIiwiYSI6ImNrZnpsamJoeDA4aTUzMXFpaXpzOXJwb2QifQ.uoBJjz-7Y-nx_jhRzK4MlA'

    this.map = new mapboxgl.Map({
      container: this.mapboxContainerRef.current,
      style: 'mapbox://styles/mapbox/streets-v11', // stylesheet location
      center: [2.835087, 41.986989], // starting position [lng, lat]
      zoom: 15 // starting zoom
    })

    this.map.on('load', () => {
      this.setState({
        mapboxInstanceLoaded: true,
      })
    })

    this.map.on('click', ({ lngLat }) => {
      const { isSelectingNewPoint, addSpot } = this.props
      console.log('...I`m being clicked lol', isSelectingNewPoint);
      if (isSelectingNewPoint) {
        const { lng, lat } = lngLat
        addSpot(lng, lat)
      }
    })

    this.setState({
      mapboxInstance: this.map,
    })
  }

  render() {
    const { isSelectingNewPoint } = this.props
    const { mapboxInstance, mapboxInstanceLoaded } = this.state

    return (
      <div className={`map ${isSelectingNewPoint ? 'is-selecting-new-point': ''}`}>
        {mapboxInstance && mapboxInstanceLoaded &&
          <Markers map={mapboxInstance} />
        }

        <div
          ref={this.mapboxContainerRef}
          className="vh-100"
        />
      </div>
    )
  }
}

export default Map
