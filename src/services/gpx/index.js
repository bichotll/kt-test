const GPX = {
  generateGPXFile (points) {
    return `
    <?xml version="1.0" encoding="UTF-8"?>
    <gpx version="1.0">
      <name>Cross country - Route builder</name>

      ${points.map(({ lat, lng, name, id }) => `
      <wpt lat="${lat}" lon="${lng}">
        <ele>${id}</ele>
        <name>${name}</name>
      </wpt>
      `).join('')}

      <trk><name>Route</name><number>1</number><trkseg>
      ${points.map(({ lat, lng, id }) => `
        <trkpt lat="${lat}" lon="${lng}"><ele>${id}</ele></trkpt>
      `).join('')}
      </trkseg></trk>

    </gpx>
    `
  },
}

export default GPX
