import GPX from './'

const p1 = {
  id: 'p1',
  name: 'p1',
  lat: 123,
  lng: 147,
}

const p2 = {
  id: 'p2',
  name: 'p2',
  lat: 456,
  lng: 258,
}

const p3 = {
  id: 'p3',
  name: 'p3',
  lat: 789,
  lng: 369,
}

const onePointArray = [
  p1,
]

const threePointsArray = [
  p1,
  p2,
  p3,
]


describe('GPX', () => {
  describe('generateGPXFile', () => {
    it('generates a gpx file content based on an array of points', () => {
      const zeroPointsFile = GPX.generateGPXFile([])
      expect(zeroPointsFile).toContain('<gpx ')
      expect(zeroPointsFile).not.toContain('<wpt ')
      expect(zeroPointsFile).not.toContain('<trkpt')

      const onePointFile = GPX.generateGPXFile([...onePointArray])
      expect(onePointFile).toContain('<gpx ')
      expect(onePointFile.split('<wpt')).toHaveLength(1 + 1)
      expect(onePointFile.split('<trkpt')).toHaveLength(1 + 1)

      const threePointsFile = GPX.generateGPXFile([...threePointsArray])
      expect(threePointsFile).toContain('<gpx ')
      expect(threePointsFile.split('<wpt')).toHaveLength(3 + 1)
      expect(threePointsFile.split('<trkpt')).toHaveLength(3 + 1)
    })
  })
})
